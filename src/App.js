import React, { useState } from "react";
import ToDoForm from "./Components/ToDoForm";
import TodoList from "./Components/ToDoList";
function App() {
  const [input, SetInput] = useState("");
  const [todos,SetTodos] =useState ([]);
  return (
    <div className="TodoApp">
      <ToDoForm todos={todos} SetTodos={SetTodos} input={input} SetInput={SetInput}/>
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
