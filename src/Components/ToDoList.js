import React,{useState} from "react";
import Todo from "./Todo";


function ToDoList({todos}) {
 
  const ustyle={listStyleType: 'none'}
  return (
    <div className="Todo-Container">
      <ul className="todo-List" style={ustyle}>
     {todos.map(todo =>(<Todo text={todo.text} key={todo.id}/>))}
      </ul>
    </div>
  );
}

export default ToDoList;
