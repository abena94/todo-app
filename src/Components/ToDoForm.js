import React, { useState } from "react";
import ToDoList from "./ToDoList";
import './ToDoForm.css'

function ToDoForm({todos,SetTodos,input,SetInput}) {
  const handleChange = (e) => {
    // 
    console.log(e.target.value);
    SetInput(e.target.value)
  };
    const handleSubmit =(e) =>{
        
        e.preventDefault();
        SetTodos([...todos,{text:input,id:Math.random()*1000},]);
        SetInput("");
    };
  return (
    <form className="ToDo-Form" >
      <input
        type="text"
        placeholder="Add task"
        name="text"
       value={input}
        className="ToDo-Input"
        onChange={handleChange}
      />
      <button className="ToDo-Button" onClick={handleSubmit}>Add</button>
    </form>
  );
}

export default ToDoForm;
