import React from 'react'

function Todo({text}) {
    const hstyle={color: 'white'}
    
    return (
        <div>
        
            <li style={hstyle} className="list-item">{text}</li>

        </div>
    )
}

export default Todo
